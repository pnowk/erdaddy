String.prototype.hashCode = function() {
    var hash = 0,
        i, chr;
    if (this.length === 0) return hash;
    for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + chr;
        hash |= 0; // Convert to 32bit integer
    }
    return hash;
};

class SharedUI {
    static get fields() {
        return {

            diagramNameInput: document.getElementById('diagramName'),
            newNodeNameInput: document.getElementById('nodeName'),
            newNodeColorInput: document.getElementById('newNodeColor'),
            newNodeTypeSimple: document.getElementById('nodeTypeSimple'),
            newNodeTypeEntity: document.getElementById('nodeTypeEntity'),

            newNodeTypeDatabase: document.getElementById('nodeTypeDatabase'),

            newNodeTypeActor: document.getElementById('nodeTypeActor'),
            newNodeTypeDecision: document.getElementById('nodeTypeDecision'),


            newNodeTypeEllipse: document.getElementById('nodeTypeEllipse'),

            newNodeTypeNote: document.getElementById('nodeTypeNote'),

            newNodeTypeProcess: document.getElementById('nodeTypeProcess'),


            selectedNodeNameInput: document.getElementById('selectedNodeName'),
            selecteNodeColorInput: document.getElementById('selectedNodeColor'),

            linkFrom: document.getElementById('optionsFrom'),

            fromPort: document.getElementById('fromPort'),
            toPort: document.getElementById('toPort'),

            linkTo: document.getElementById('optionsTo'),
            linkLabelInput: document.getElementById('linkLabel'),
            relType: document.getElementById('relationshipType'),

            diagramList: document.getElementById('diagramList'),
            divID: document.getElementById("myDiagramDiv"),

            openNav: document.getElementById('openNav'),
            closeNav: document.getElementById('closeNav')

        }
    }

    static get resources() {
        return {
            dataTypes: ["Number", 'Text', 'Date', 'Blob', 'Bool', 'Method', 'Field'],
            deleteItemHTML: "<i class='fa fa-trash' aria-hidden='true'></i>",
            appendItemHTML: "<i class='fas fa-plus'> New Item</i>"

        }

    }

    static get colors() {
        return {
            bluegrad: "rgb(86, 86, 186)",
            greengrad: "rgb(158, 209, 159)",
            redgrad: "rgb(206, 106, 100)",
            yellowgrad: "rgb(254, 221, 50)",
            lightgrad: "#E6E6FA"
        }
    }

    static get components() {
        return {
            sidenav: document.getElementById('sidenav'),
            main: document.getElementById('main'),
            cover: document.getElementById('cover'),
            diagram: document.getElementById("myDiagramDiv"),
            selectedNodeItemsSection: document.getElementById('selectedNodeItemsSection'),
            savedModel: document.getElementById('savedModel'),
            customEventContainer: document.getElementById('customEventContainer'),
            nodeItems: document.getElementsByClassName('nodeItem'),
            itemProperties: document.getElementsByClassName('itemProperty'),

            newNodeData: {
                key: this.fields.newNodeNameInput.value,
                color: this.fields.newNodeColorInput.value,
                simple: this.fields.newNodeTypeSimple.checked === true,
                entity: this.fields.newNodeTypeEntity.checked === true,
                database: this.fields.newNodeTypeDatabase.checked === true,
                actor: this.fields.newNodeTypeActor.checked === true,
                decision: this.fields.newNodeTypeDecision.checked === true,
                ellipse: this.fields.newNodeTypeEllipse.checked === true,
                note: this.fields.newNodeTypeNote.checked === true,
                process: this.fields.newNodeTypeProcess.checked === true,
                loc: "0 0"
            },

            linkData: {
                from: this.fields.linkFrom.value,
                to: this.fields.linkTo.value,
                relType: this.fields.relType.value,
                linkLabel: this.fields.linkLabelInput.value,
                fromPort: this.fields.fromPort.value,
                toPort: this.fields.toPort.value

            },
            topMenu: {
                newDiagramLink: document.getElementById('newDiagramLink'),
                cloneDiagramLink: document.getElementById('cloneDiagramLink'),
                saveToLocalStorage: document.getElementById('saveToLocalStorageLink'),
                removeDiagramLink: document.getElementById('removeDiagramLink'),

                saveToFileLink: document.getElementById('saveToFileLink'),


                fileDialog: document.getElementById('fileDialog'),
                open: document.getElementById('openExpand'),
                openContent: document.getElementById('openExpandContent'),
                openSubMenu: document.querySelector('.dropdown-submenu')

            },

            side: {
                selectedNodeSection: document.getElementById('selectedNodeSection'),
                relationshipSection: document.getElementById('relationshipSection'),






            },


            buttons: {
                newDiagram: document.getElementById('newDiagram'),
                loadDiagram: document.getElementById('loadDiagram'),
                removeDiagram: document.getElementById('removeDiagram'),
                saveDiagram: document.getElementById('saveDiagram'),
                addNode: document.getElementById('addNode'),
                removeNode: document.getElementById('removeNode'),
                updateNode: document.getElementById('saveNode'),
                addLink: document.getElementById('addLink'),
                updateLink: document.getElementById('updateLink'),

                openNav: document.getElementById('openNav'),
                closeNav: document.getElementById('closeNav')
            }

        }

    }


    static closeSidenav() {
        this.components.sidenav.style.width = "0";
        this.components.main.style.marginLeft = "1px";
        this.components.buttons.openNav.style.display = "block";
        this.components.cover.style.left = "1px";
    }

    static openSidenav() {
        this.components.sidenav.style.width = "240px";
        this.components.main.style.marginLeft = "240px";
        this.components.buttons.openNav.style.display = "none";
        this.components.cover.style.left = "240px";
    }

    static resetHighlight() {
        this.components.side.relationshipSection.style.background = "#f2f2f2";
        this.components.side.selectedNodeSection.style.background = "#f2f2f2";
    }

    static highlightNodeSection() {
        this.resetHighlight();
        this.components.side.selectedNodeSection.style.background = "#F0E68C";
        // this.components.side.selectedNodeSection.style.border = "1px solid silver";

    }

    static highlightLinkSection() {
        this.resetHighlight();
        // this.components.side.relationshipSection.style.border = "1px solid silver";
        this.components.side.relationshipSection.style.background = "#F0E68C";
    }

    static retrieveNodeItems() {
        var items = [];

        var nodeItems = this.components.nodeItems;

        for (var i = 0; i < nodeItems.length; i++) {
            var item = nodeItems[i];
            var children = item.children;
            var name = children[0].value;
            var dataType = children[1].value;
            var isKey = children[2].checked;
            var nodeItem = new NodeItem(name, dataType, isKey);

            items.push(nodeItem);
        }
        return items;
    }

    static prepareEmptyRow() {

        var id = Math.floor(-1 * (Math.random(0, 1) * 100 + 1));
        var section = this.components.selectedNodeItemsSection;
        var row = document.createElement('div');
        row.id = id;
        row.className = "row nodeItem";
        var nameInput = document.createElement('input');

        //nameInput.id = item.name;
        //nameInput.value = item.name;
        row.appendChild(nameInput);
        var typeSelect = document.createElement('select');
        var options = this.resources.dataTypes; // ["Number", 'Text', 'Date', 'Blob', 'Bool'];
        options.forEach(function(o) {
            var opt = document.createElement('option');
            opt.value = o;
            opt.innerHTML = o;

            typeSelect.appendChild(opt);
        })

        //typeSelect.value = item.dataType;
        row.appendChild(typeSelect);

        var chk = document.createElement('input');
        chk.type = 'checkbox';
        //chk.checked = item.iskey;
        //console.log(item, typeof(item.iskey))

        row.appendChild(chk);

        var btnRemove = document.createElement('button');
        btnRemove.type = "button";
        btnRemove.innerHTML = this.resources.deleteItemHTML;
        btnRemove.addEventListener('click', function() {

            section.removeChild(row);
        })

        row.appendChild(btnRemove);

        //section.appendChild(row);
        return row;
    }

    static truncatePortLists() {
        this.fields.fromPort.length = 0;
        this.fields.toPort.length = 0;
    }

    static refreshFromPorts(items) {
        var fromPort = this.fields.fromPort;

        if (items) {

            if (items.length > 0) {

                fromPort.length = 0;
                items.forEach(function(item) {
                    var opt1 = document.createElement('option');
                    opt1.value = item.name;
                    opt1.innerHTML = item.name;
                    fromPort.appendChild(opt1);
                })

                fromPort.selectedIndex = -1;
            }
        } else {

            fromPort.length = 0;
            fromPort.selectedIndex = -1;
        }
    }


    static refreshToPorts(items) {
        var toPort = this.fields.toPort;
        if (items) {

            if (items.length > 0) {
                toPort.length = 0;
                items.forEach(function(item) {
                    var opt1 = document.createElement('option');
                    opt1.value = item.name;
                    opt1.innerHTML = item.name;
                    toPort.appendChild(opt1);
                })

                toPort.selectedIndex = -1;
            }
        } else {
            toPort.length = 0;
            toPort.selectedIndex = -1;
        }
    }

    static pushNodeItems(items, nodeData) {
        var section = this.components.selectedNodeItemsSection;
        section.innerHTML = "";
        var self = this;

        //create a 'new' button in the selected node items section 
        //so the user can add new item if this entity node clicked
        if (nodeData.entity) {
            //append button to create new  row
            var btnAppend = document.createElement('button');
            section.appendChild(btnAppend);
            btnAppend.type = 'button';
            btnAppend.className = "btn btn-info btn-sm"
                // btnAppend.innerText = 'New Item';
            btnAppend.innerHTML = this.resources.appendItemHTML;
            btnAppend.addEventListener('click', function() {
                section.appendChild(SharedUI.prepareEmptyRow())
            });


            var note = document.createElement('p');
            note.innerText = "* field name, data type, is key";
            section.appendChild(note);
        }




        if (items.length > 0) {
            //row.innerHTML = items;
            var input = document.createElement('input');
            items.forEach(function(item) {
                var row = document.createElement('div');
                row.id = "item" + item.name;
                row.className = "row nodeItem";
                var nameInput = document.createElement('input');
                nameInput.id = item.name;
                nameInput.value = item.name;
                nameInput.className = 'itemProperty itemName';
                row.appendChild(nameInput);
                var typeSelect = document.createElement('select');
                typeSelect.className = 'itemProperty itemDataType';
                var options = self.resources.dataTypes; // ["Number", 'Text', 'Date', 'Blob'];
                options.forEach(function(o) {
                    var opt = document.createElement('option');
                    opt.value = o;
                    opt.innerHTML = o;

                    typeSelect.appendChild(opt);
                })

                typeSelect.value = item.dataType;
                row.appendChild(typeSelect);

                var chk = document.createElement('input');
                chk.className = 'itemProperty itemIsKey';
                chk.type = 'checkbox';
                chk.checked = item.isKey;
                //console.log(item, typeof(item.iskey))

                row.appendChild(chk);

                var btnRemove = document.createElement('button');
                btnRemove.type = "button";
                //btnRemove.innerHTML = "Delete";
                //btnRemove.className = "btn btn-danger btn-sm";
                btnRemove.innerHTML = self.resources.deleteItemHTML;
                btnRemove.addEventListener('click', function() {
                    console.log('removing', row.id);
                    section.removeChild(row);
                })

                row.appendChild(btnRemove);

                section.appendChild(row);
            })

        }


    }

    static refreshNodeLists(nodes) {
        var linkFrom = this.fields.linkFrom;
        var linkTo = this.fields.linkTo;
        linkFrom.length = 0;
        linkTo.length = 0;
        nodes.forEach(function(n) {

            if (n.category !== 'note') { //exclude note from dropdowns

                var opt1 = document.createElement("option");
                var opt2 = document.createElement("option");
                if (n.isGroup) {
                    opt1.innerHTML = n.text;
                    opt1.value = n.key;
                    opt2.innerHTML = n.text;
                    opt2.value = n.key;
                } else {

                    opt1.innerHTML = n.key;
                    opt1.value = n.key;
                    opt2.innerHTML = n.key;
                    opt2.value = n.key;
                }
                linkFrom.appendChild(opt1);
                linkTo.appendChild(opt2);
            }

        });

        linkFrom.selectedIndex = -1;
        linkTo.selectedIndex = -1;


    }

    static updateSavedModel(modelJson) {
        this.components.savedModel.textContent = modelJson;
    }

    static reset() {
        this.fields.diagramNameInput.value = '';
        this.fields.newNodeNameInput.value = '';
        this.fields.selectedNodeNameInput.value = '';
        this.fields.linkLabelInput.value = '';
        this.components.selectedNodeItemsSection.innerHTML = '';
    }
}