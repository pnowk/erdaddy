class StorageGateway {

    static fetchDiagrams() {
        //console.log('fetching diagrams')
        var storage = window.localStorage;
        var items = [];
        for (let key in storage) {
            let item = storage.getItem(key);
            if (item !== null) {
                items.push(key);
            }

        }
        return items;
    }

    static removeDiagramByKey(key) {
        //window.localStorage.setItem(key, null);
        window.localStorage.removeItem(key);
    }

    static getDiagramData(diagramKey) {
        return window.localStorage.getItem(diagramKey);
        // diagram.model = go.Model.fromJson(localStorage.getItem(diagramKey));
    }

    static storeDiagram(key, data) {
        window.localStorage.setItem(key, data);
    }

    static test() {
        console.log('storage gateway test');
    }


    static loadFromFile(file) {};


    static saveToFile(diagramModelJson, target) {

        var data = new Blob([diagramModelJson]);
        target.href = URL.createObjectURL(data);
    }
}


class DiagramService {

    constructor(diagramElement) {

        this.nodeFactory = new NodeFactory();
        this.linkFactory = new LinkFactory();
        var $ = go.GraphObject.make;
        this.diagram = $(go.Diagram, diagramElement.id, {
            "undoManager.isEnabled": true,
            "commandHandler.archetypeGroupData": { text: "Group", isGroup: true, color: "blue" },

        });

        this.diagram.model = $(go.GraphLinksModel, {

            linkFromPortIdProperty: "fromPort", // required information:
            linkToPortIdProperty: "toPort"
        });


        // To simplify this code we define a function for creating a context menu button:
        function makeButton(text, action, visiblePredicate) {
            return $("ContextMenuButton",
                $(go.TextBlock, text), { click: action },
                // don't bother with binding GraphObject.visible if there's no predicate
                visiblePredicate ? new go.Binding("visible", "", function(o, e) { return o.diagram ? visiblePredicate(o, e) : false; }).ofObject() : {});
        }


        // a context menu is an Adornment with a bunch of buttons in them
        var partContextMenu =
            $("ContextMenu",
                makeButton("Properties",
                    function(e, obj) { // OBJ is this Button
                        var contextmenu = obj.part; // the Button is in the context menu Adornment
                        var part = contextmenu.adornedPart; // the adornedPart is the Part that the context menu adorns
                        // now can do something with PART, or with its data, or with the Adornment (the context menu)
                        if (part instanceof go.Link) alert(linkInfo(part.data));
                        else if (part instanceof go.Group) alert(groupInfo(contextmenu));
                        else alert(nodeInfo(part.data));
                    }),
                makeButton("Cut",
                    function(e, obj) { e.diagram.commandHandler.cutSelection(); },
                    function(o) { return o.diagram.commandHandler.canCutSelection(); }),
                makeButton("Copy",
                    function(e, obj) { e.diagram.commandHandler.copySelection(); },
                    function(o) { return o.diagram.commandHandler.canCopySelection(); }),
                makeButton("Paste",
                    function(e, obj) { e.diagram.commandHandler.pasteSelection(e.diagram.lastInput.documentPoint); },
                    function(o) { return o.diagram.commandHandler.canPasteSelection(); }),
                makeButton("Delete",
                    function(e, obj) { e.diagram.commandHandler.deleteSelection(); },
                    function(o) { return o.diagram.commandHandler.canDeleteSelection(); }),
                makeButton("Undo",
                    function(e, obj) { e.diagram.commandHandler.undo(); },
                    function(o) { return o.diagram.commandHandler.canUndo(); }),
                makeButton("Redo",
                    function(e, obj) { e.diagram.commandHandler.redo(); },
                    function(o) { return o.diagram.commandHandler.canRedo(); }),
                makeButton("Group",
                    function(e, obj) { e.diagram.commandHandler.groupSelection(); },
                    function(o) { return o.diagram.commandHandler.canGroupSelection(); }),
                makeButton("Ungroup",
                    function(e, obj) { e.diagram.commandHandler.ungroupSelection(); },
                    function(o) { return o.diagram.commandHandler.canUngroupSelection(); })
            );

        function nodeInfo(d) { // Tooltip info for a node data object
            var str = "Node " + d.key + ": " + d.text + "\n";
            if (d.group)
                str += "member of " + d.group;
            else
                str += "top-level node";
            return str;
        }


        function groupInfo(adornment) { // takes the tooltip or context menu, not a group node data object
            var g = adornment.adornedPart; // get the Group that the tooltip adorns
            var mems = g.memberParts.count;
            var links = 0;
            g.memberParts.each(function(part) {
                if (part instanceof go.Link) links++;
            });
            return "Group " + g.data.key + ": " + g.data.text + "\n" + mems + " members including " + links + " links";
        }

        // Groups consist of a title in the color given by the group node data
        // above a translucent gray rectangle surrounding the member parts
        this.diagram.groupTemplate =
            $(go.Group, "Vertical", {
                    selectionObjectName: "PANEL", // selection handle goes around shape, not label
                    ungroupable: true // enable Ctrl-Shift-G to ungroup a selected Group
                },
                $(go.TextBlock, {
                        font: "bold 19px sans-serif",
                        isMultiline: false, // don't allow newlines in text
                        editable: true // allow in-place editing by user
                    },
                    new go.Binding("text", "text").makeTwoWay(),
                    new go.Binding("stroke", "color")),
                $(go.Panel, "Auto", { name: "PANEL" },
                    $(go.Shape, "Rectangle", // the rectangular shape around the members
                        {
                            fill: "rgba(128,128,128,0.2)",
                            stroke: "gray",
                            strokeWidth: 3,
                            portId: "",
                            cursor: "pointer", // the Shape is the port, not the whole Node
                            // allow all kinds of links from and to this port
                            fromLinkable: true,
                            fromLinkableSelfNode: true,
                            fromLinkableDuplicates: true,
                            toLinkable: true,
                            toLinkableSelfNode: true,
                            toLinkableDuplicates: true
                        }),
                    $(go.Placeholder, { margin: 10, background: "transparent" }) // represents where the members are
                ), { // this tooltip Adornment is shared by all groups
                    toolTip: $("ToolTip",
                        $(go.TextBlock, { margin: 4 },
                            // bind to tooltip, not to Group.data, to allow access to Group properties
                            new go.Binding("text", "", groupInfo).ofObject())
                    ),
                    // the same context menu Adornment is shared by all groups
                    contextMenu: partContextMenu
                }
            );

        // Define the behavior for the Diagram background:

        function diagramInfo(model) { // Tooltip info for the diagram's model
            return "Model:\n" + model.nodeDataArray.length + " nodes, " + model.linkDataArray.length + " links";
        }

        // provide a tooltip for the background of the Diagram, when not over any Part
        this.diagram.toolTip =
            $("ToolTip",
                $(go.TextBlock, { margin: 4 },
                    new go.Binding("text", "", diagramInfo))
            );



        // provide a context menu for the background of the Diagram, when not over any Part
        this.diagram.contextMenu =
            $("ContextMenu",
                makeButton("Paste",
                    function(e, obj) { e.diagram.commandHandler.pasteSelection(e.diagram.lastInput.documentPoint); },
                    function(o) { return o.diagram.commandHandler.canPasteSelection(); }),
                makeButton("Undo",
                    function(e, obj) { e.diagram.commandHandler.undo(); },
                    function(o) { return o.diagram.commandHandler.canUndo(); }),
                makeButton("Redo",
                    function(e, obj) { e.diagram.commandHandler.redo(); },
                    function(o) { return o.diagram.commandHandler.canRedo(); })
            );


        var itemTempl =
            $(go.Panel, "Horizontal", {
                    //toSpot: go.Spot.LeftSide, //new go.Spot(-1.2, 0.0, 0, 7),
                    toSpot: new go.Spot(-0.13, 0.5),
                    //toSpot: new go.Spot(-0.2, 0.0, 0, 7),
                    //fromSpot: go.Spot.RightSide, //s new go.Spot(7.5, 0.0, 0, 7),
                    fromSpot: new go.Spot(1.15, 0.5),
                    margin: new go.Margin(1, 0, 0, 0),
                    stretch: go.GraphObject.Fill,
                    defaultAlignment: go.Spot.Center
                },
                new go.Binding("portId", "portId"),
                $(go.Shape, {
                        desiredSize: new go.Size(10, 10),
                        margin: new go.Margin(0, 2, 0, 0)



                    },
                    new go.Binding("figure", "figure"),
                    new go.Binding("fill", "color"),


                ),
                $(go.TextBlock, {
                        stroke: "#333333",
                        font: "bold 10px sans-serif"

                    },
                    new go.Binding("text", "name"))
            );

        var templ = new go.Map();

        // this.diagram.nodeTemplate // provide custom Node appearance

        var entityTemplate =
            $(go.Node, "Auto", {

                    selectionAdorned: true,
                    // resizable: true,
                    isShadowed: true,
                    shadowColor: "#C5C1AA",
                    cursor: "pointer"

                },

                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, { figure: "RoundedRectangle", fill: "#EFEBCA" },
                    new go.Binding("fill", "color"),
                    new go.Binding('figure', 'figure')

                ),

                $(go.Panel, "Table", { margin: 8, stretch: go.GraphObject.Fill },
                    $(go.RowColumnDefinition, { row: 0, sizing: go.RowColumnDefinition.None }),
                    // the table header
                    $(go.TextBlock, {
                            row: 0,
                            alignment: go.Spot.Center,
                            margin: new go.Margin(0, 14, 0, 2), // leave room for Button
                            font: "bold 13px sans-serif"
                        },
                        new go.Binding("text", "key")),
                    // the collapse/expand button
                    $("PanelExpanderButton", "LIST", // the name of the element whose visibility this button toggles
                        { row: 0, alignment: go.Spot.TopRight }),
                    // the list of Panels, each showing an attribute
                    $(go.Panel, "Vertical", {
                            name: "LIST",
                            row: 1,

                            padding: 1,
                            alignment: go.Spot.TopLeft,

                            defaultAlignment: go.Spot.Left,
                            stretch: go.GraphObject.Horizontal,
                            itemTemplate: itemTempl,
                            // ""
                        },
                        new go.Binding("itemArray", "items")


                    )
                ),

                { // this tooltip Adornment is shared by all nodes
                    toolTip: $("ToolTip",
                        $(go.TextBlock, { margin: 4 }, // the tooltip shows the result of calling nodeInfo(data)
                            new go.Binding("text", "", nodeInfo))
                    ),
                    // this context menu Adornment is shared by all nodes
                    contextMenu: partContextMenu
                }

                // $(go.TextBlock, {
                //         text: "hello!",
                //         margin: 5,
                //         font: "14px Verdana"
                //     },
                //     new go.Binding("text", "key")
                // )
            );


        var simpleTemplate =
            $(go.Node, "Auto", {

                    selectionAdorned: true,
                    // resizable: true,
                    isShadowed: true,
                    shadowColor: "#C5C1AA",
                    cursor: "pointer"

                },

                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, { figure: "RoundedRectangle", fill: "#EFEBCA" },
                    new go.Binding("fill", "color"),
                    new go.Binding('figure', 'figure')

                ),


                $(go.TextBlock, {
                        text: "hello!",
                        margin: 5,
                        font: "16px Verdana"
                    },
                    new go.Binding("text", "key")
                ), { // this tooltip Adornment is shared by all nodes
                    toolTip: $("ToolTip",
                        $(go.TextBlock, { margin: 4 }, // the tooltip shows the result of calling nodeInfo(data)
                            new go.Binding("text", "", nodeInfo))
                    ),
                    // this context menu Adornment is shared by all nodes
                    contextMenu: partContextMenu
                }


            );



        var actorTemplate =
            $(go.Node, "Table", {

                    selectionAdorned: true,
                    // resizable: true,
                    isShadowed: true,
                    shadowColor: "#C5C1AA",
                    cursor: "pointer"

                },

                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, {
                        figure: "BpmnTaskUser",
                        fill: "#EFEBCA",
                        width: 60,
                        height: 40,
                        row: 0,
                        column: 0
                    },
                    new go.Binding("fill", "color"),
                    new go.Binding('figure', 'figure')

                ),


                $(go.TextBlock, {
                        row: 1,
                        column: 0,
                        text: "hello!",
                        margin: new go.Margin(0, 0, 0, 0),
                        font: "14px Verdana"
                    },
                    new go.Binding("text", "key")
                ), { // this tooltip Adornment is shared by all nodes
                    toolTip: $("ToolTip",
                        $(go.TextBlock, { margin: 4 }, // the tooltip shows the result of calling nodeInfo(data)
                            new go.Binding("text", "", nodeInfo))
                    ),
                    // this context menu Adornment is shared by all nodes
                    contextMenu: partContextMenu
                }


            );


        var noteTemplate =
            $(go.Node, "Auto", {

                },
                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, { figure: "Rectangle", fill: "white" },
                    new go.Binding("fill", "color")


                ),
                $(go.TextBlock, {
                        //text: "hello!",
                        margin: 5,
                        font: "9px Verdana",
                        isMultiline: true, // don't allow newlines in text
                        editable: true,

                    },
                    new go.Binding("text", "note").makeTwoWay(),

                )


            );




        this.diagram.nodeTemplate =
            $(go.Node, "Auto", {

                    selectionAdorned: true,
                    // resizable: true,
                    isShadowed: true,
                    shadowColor: "#C5C1AA",
                    cursor: "pointer"

                },

                new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
                $(go.Shape, { figure: "Rectangle", fill: "#EFEBCA" },
                    new go.Binding("fill", "color"),
                    new go.Binding('figure', 'figure')

                ),


                $(go.TextBlock, {
                        text: "hello!",
                        margin: 5,
                        font: "12px Verdana",

                        editable: true
                    },
                    new go.Binding("text", "key")
                ), { // this tooltip Adornment is shared by all nodes
                    toolTip: $("ToolTip",
                        $(go.TextBlock, { margin: 4 }, // the tooltip shows the result of calling nodeInfo(data)
                            new go.Binding("text", "", nodeInfo))
                    ),
                    // this context menu Adornment is shared by all nodes
                    contextMenu: partContextMenu
                }


            );



        templ.add('entity', entityTemplate);
        templ.add('simple', simpleTemplate);
        templ.add('actor', actorTemplate);
        templ.add('note', noteTemplate);
        templ.add("", this.diagram.nodeTemplate);


        this.diagram.nodeTemplateMap = templ;


        this.diagram.addDiagramListener('ObjectSingleClicked', function(e) {
            var part = e.subject.part;
            if (!(part instanceof go.Link)) { //edditing node
                var nodeClicked = new CustomEvent('nodeClicked', { detail: { nodeData: part.data } });
                SharedUI.components.customEventContainer.dispatchEvent(nodeClicked);

            } else { //editing link
                var linkClicked = new CustomEvent('linkClicked', { detail: { linkData: part.data } });
                SharedUI.components.customEventContainer.dispatchEvent(linkClicked);
            }
        });

        //model changed
        this.diagram.addModelChangedListener(function(e) {
            if (e.isTransactionFinished) {
                var event = new CustomEvent('diagramChanged', { detail: { model: e.model } });
                diagramElement.dispatchEvent(event);
            };
        })

        this.diagram.linkTemplate = $(go.Link, {
                routing: go.Link.AvoidsNodes,
                curve: go.Link.JumpOver,
                corner: 5,
                cursor: "pointer"


            },
            $(go.Shape),
            $(go.Shape, new go.Binding("fromArrow", "fromArrow"), {
                scale: 2,
                // fill: "red",
                segmentIndex: 1,
                // stroke: "#1967B3",
                //segmentOffset: new go.Point(NaN, NaN),
                segmentFraction: 0.4,
                // fill: "#D4B52C"

            }),
            $(go.Shape, new go.Binding("toArrow", "toArrow"), {
                scale: 2,
                // fill: "#D4B52C",
                segmentIndex: 1,
                // stroke: "#1967B3",
                //segmentOffset: new go.Point(NaN, NaN),
                segmentFraction: 0.4,
                // fill: "white"

                //fill: "#D4B52C"
            }, new go.Binding("fill", "fill")),
            $(go.TextBlock, new go.Binding("text", "fromText"), {
                segmentIndex: 1,
                stroke: "#1967B3",
                segmentOffset: new go.Point(NaN, NaN),
                segmentFraction: 0.5,
                font: "bold 8pt serif"
            }),
            $(go.TextBlock, new go.Binding("text", "linkLabel"), {
                segmentIndex: 2,
                segmentFraction: 0.5,
                stroke: "#1967B3",
                segmentOffset: new go.Point(0, -15)
            }, { editable: true }),
            $(go.TextBlock, new go.Binding("text", "toText"), {
                segmentIndex: 3,
                stroke: "#1967B3",
                segmentOffset: new go.Point(NaN, NaN),
                segmentFraction: 0.5,
                font: "bold 8pt serif"

            }, { editable: true })

        );
    }

    getDiagram() {
        return this.diagram;
    }

    appendNode(n) {
        var nodeData = n;

        if (nodeData.items === undefined) {
            nodeData.items = [];
        }

        var node;
        node = this.nodeFactory.createNode(nodeData);
        this.diagram.model.addNodeData(node);
    }

    createLink(linkData) {

        var link = this.linkFactory.createLink(linkData);
        this.diagram.model.addLinkData(link);
    }

    updateLinkByKey(key, linkData) {

        var updatedLink = this.linkFactory.createLink(linkData);

        var link = this.diagram.findLinksByExample({ key: key }).first();

        if (link) {
            this.diagram.model.setDataProperty(link.data, "from", updatedLink.from);
            this.diagram.model.setDataProperty(link.data, "to", updatedLink.to);
            this.diagram.model.setDataProperty(link.data, "linkLabel", updatedLink.linkLabel);
            this.diagram.model.setDataProperty(link.data, "relType", updatedLink.relType);
            this.diagram.model.setDataProperty(link.data, "fromText", updatedLink.fromText);
            this.diagram.model.setDataProperty(link.data, "toText", updatedLink.toText);
            this.diagram.model.setDataProperty(link.data, "fromArrow", updatedLink.fromArrow);
            this.diagram.model.setDataProperty(link.data, "toArrow", updatedLink.toArrow);
            this.diagram.model.setDataProperty(link.data, "key", updatedLink.key);
            this.diagram.model.setDataProperty(link.data, "fill", updatedLink.fill);
            this.diagram.model.setDataProperty(link.data, "fromPort", updatedLink.fromPort);
            this.diagram.model.setDataProperty(link.data, "toPort", updatedLink.toPort);
        }
    }

    removeNodeByKey(key) {
        var node = this.diagram.model.findNodeDataForKey(key);

        if (node !== null) this.diagram.model.removeNodeData(node);
    }

    updateNodeByKey(keyToUpdate, newNodeData) {

        var newNode = this.nodeFactory.createNode(newNodeData);


        var data = this.diagram.model.findNodeDataForKey(keyToUpdate);
        if (data != null) {
            this.diagram.model.setDataProperty(data, "key", newNode.key);
            this.diagram.model.setDataProperty(data, "color", newNode.color);
            this.diagram.model.setDataProperty(data, "simple", newNode.simple);
            this.diagram.model.setDataProperty(data, "entity", newNode.entity);
            this.diagram.model.setDataProperty(data, "items", newNode.items);
        }
    }

    getNodeByKey(key) {
        var data = this.diagram.model.findNodeDataForKey(key);
        return data;
    }

    loadDiagram(diagramKey) {
        //console.log(this, 'load diagram', diagramKey)
        this.diagram.model = go.Model.fromJson(StorageGateway.getDiagramData(diagramKey));
    }

    loadFromModel(model) {
        this.diagram.model = go.Model.fromJson(model);
    }

    removeDiagramByKey(diagramKey) {
        // this.diagram.model = new go.GraphLinksModel();
        this.diagram.model = go.GraphObject.make(go.GraphLinksModel, {

            linkFromPortIdProperty: "fromPort", // required information:
            linkToPortIdProperty: "toPort"
        })

        StorageGateway.removeDiagramByKey(diagramKey);
    }

    removeAllDiagrams() {
        //console.trace('removing all diagrams')
        var self = this;
        var diagrams = this.fetchDiagrams();
        diagrams.forEach(function(d) {
            //this.removeDiagram(d);

            self.removeDiagramByKey(d);
            // a.refreshAvailableDiagrams()
        })
    }

    storeDiagram(diagramKey) {
        StorageGateway.storeDiagram(diagramKey, this.diagram.model.toJson());
    }


    saveToFile(diagramModelJson, target) {
        StorageGateway.saveToFile(diagramModelJson, target);
    }

    clearModel() {
        this.diagram.model = go.GraphObject.make(go.GraphLinksModel, {

            linkFromPortIdProperty: "fromPort",
            linkToPortIdProperty: "toPort"
        })

    }

    fetchDiagrams() {
        return StorageGateway.fetchDiagrams();
    }

    fetchNodes() {
        return this.diagram.model.nodeDataArray;
    }

}


function NodeFactory() {
    this.createNode = function(nodeData) {
        var node;
        var items = nodeData.items;

        if (nodeData.simple) {
            node = new SimpleNode(items);
        } else if (nodeData.entity) {
            node = new EntityNode(items);
        } else if (nodeData.database) {
            node = new DatabaseNode(items);
        } else if (nodeData.actor) {
            node = new ActorNode(items);
        } else if (nodeData.decision) {
            node = new DecisionNode(items);
        } else if (nodeData.ellipse) {
            node = new EllipseNode(items);
        } else if (nodeData.note) {
            node = new NoteNode(items, nodeData.key);
        } else if (nodeData.process) {
            node = new ProcessNode(items);
        } else {
            node = new EntityNode(items);
        }
        node.simple = nodeData.simple;
        node.entity = nodeData.entity;
        node.color = nodeData.color;
        node.key = nodeData.key;
        node.loc = nodeData.loc;

        return node;
    }
}

var SimpleNode = function(items) {
    this.category = "simple";
    this.items = [];
    this.figure = 'Rectangle';
};
var EntityNode = function(items) {
    this.category = "entity";
    this.items = items;
    this.figure = 'RoundedRectangle';
}

var DatabaseNode = function(items) {
    console.trace(this)
    this.category = "simple";
    this.items = [];
    this.figure = 'Database';
}

var DecisionNode = function(items) {
    this.category = "simple"
    console.trace(this)
    this.items = [];
    this.figure = 'Diamond';
}

var ActorNode = function(items) {
    console.trace(this);
    this.category = "actor";
    this.items = [];
    // this.figure = 'BpmnTaskUser';
}


var EllipseNode = function(items) {
    this.category = "simple";
    console.trace(this)
    this.items = [];
    this.figure = 'Ellipse';

}

var NoteNode = function(items, note) {
    console.trace(this)
    this.category = "note"
    this.items = [];
    this.note = note;
    // this.figure = 'SpeechBubble';



}

var ProcessNode = function(items) {
    console.trace(this)
    this.category = "simple"
    this.items = [];
    this.figure = 'Process';

}

function LinkFactory() {
    this.createLink = function(linkData) {
        var link;

        switch (linkData.relType) {
            case "1":
                link = new OneToMany();
                break;
            case "2":
                link = new OneToOne();
                break;
            case "4":
                link = new ManyToMany();
                break;
            case "5":
                link = new PlainLine();
                break;
            case "6":
                link = new Standard();
                break;
            case "7":
                link = new Association();
                break;
            case "8":
                link = new Aggregation();
                break;
            case "9":
                link = new Composition();
                break;
            case "10":
                link = new Generalization();
                break;
            default:
                link = new PlainLine();

        }

        link.fill = (linkData.relType === "9") ? "black" : "white";
        link.from = linkData.from;
        link.fromPort = linkData.fromPort;
        link.to = linkData.to;
        link.toPort = linkData.toPort;
        link.relType = linkData.relType;
        link.linkLabel = linkData.linkLabel;
        link.key = (linkData.from + linkData.to + linkData.relType + linkData.linkLabel).hashCode();

        return link;
    }
}


var PlainLine = function() {
    this.fromText = "";
    this.toText = "";
    this.fromArrow = "";
    this.toArrow = "";

}

var OneToMany = function() {
    this.fromText = "1";
    this.toText = "0..N";
    this.fromArrow = "Line";
    this.toArrow = "Fork";
}

var OneToOne = function() {
    this.fromText = "1";
    this.toText = "1";
    this.fromArrow = "Line";
    this.toArrow = "Line";
}

var ManyToMany = function() {
    this.fromText = "0..N";
    this.toText = "0..N";

    this.fromArrow = "BackwardFork";
    this.toArrow = "Fork";

}

var Standard = function() {
    this.fromText = "";
    this.toText = "";

    this.fromArrow = "";
    this.toArrow = "Feather";
}

var Association = function() {
    this.fromText = "";
    this.toText = "";

    this.fromArrow = "";
    this.toArrow = "";
}

var Aggregation = function() {
    this.fromText = "";
    this.toText = "";


    this.fromArrow = "";
    this.toArrow = "StretchedDiamond";
}


var Composition = function() {
    this.fromText = "";
    this.toText = "";

    this.fromArrow = "";
    this.toArrow = "StretchedDiamond";
}



var Generalization = function() {
    this.fromText = "";
    this.toText = "";

    this.fromArrow = "";
    this.toArrow = "Triangle";
}



class NodeItem {

    constructor(name, dataType, isKey) {
        //console.log('node item args', name, dataType, isKey)
        this.name = name;
        this.dataType = dataType;
        this.isKey = isKey;

        this.portId = name;

        if (dataType === "Text") {
            this.figure = "Cube1";
            this.color = SharedUI.colors.bluegrad;
        } else if (dataType === "Number") {
            this.figure = "Diamond";
            this.color = SharedUI.colors.redgrad;

        } else if (dataType === "Bool") {
            this.figure = "MinusLine";
            this.color = SharedUI.colors.redgrad;

        } else if (dataType === "Method" || dataType === "Field") {
            this.figure = "PlusLine";
            this.color = SharedUI.colors.redgrad;

        } else {
            this.figure = "Circle";
            this.color = SharedUI.colors.lightgrad;
        }

        if (isKey) {
            this.figure = "Key";
            this.color = SharedUI.colors.yellowgrad;
        }
    }
}