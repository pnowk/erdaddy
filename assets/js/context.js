class AppContext {

    constructor() {
        this.diagramService = new DiagramService(SharedUI.components.diagram);
        this.nodeFactory = new NodeFactory(); //only to enable building examples
        this.linkFactory = new LinkFactory(); //only to enable building examples
        this.current;
        this.currentLink;
    }

    set dirty(node) {
        this.current = node;
    }

    set dirtyLink(link) {
        this.currentLink = link;
    }

    get appState() {
        return {
            isDiagramLoaded: SharedUI.fields.diagramNameInput.value !== ''

        }
    }

    get currentDiagramName() {
        return SharedUI.fields.diagramNameInput.value;
    }

    get selectedDiagram() {
        return SharedUI.fields.diagramList.value;
    }

    registerEvents() {
        SharedUI.components.customEventContainer.addEventListener('nodeClicked', this.handle.nodeClicked);
        SharedUI.components.customEventContainer.addEventListener('linkClicked', this.handle.linkClicked);


        SharedUI.components.buttons.newDiagram.addEventListener('click', this.handle.newDiagramClicked);
        // SharedUI.components.buttons.loadDiagram.addEventListener('click', this.handle.loadDiagramClicked);
        SharedUI.components.buttons.saveDiagram.addEventListener('click', this.handle.saveDiagramClicked);
        SharedUI.components.buttons.removeDiagram.addEventListener('click', this.handle.removeDiagramClicked);
        SharedUI.components.diagram.addEventListener('diagramChanged', this.handle.diagramChanged);
        SharedUI.components.buttons.addNode.addEventListener('click', this.handle.addNodeClicked);
        SharedUI.components.buttons.removeNode.addEventListener('click', this.handle.removeNodeClicked);
        SharedUI.components.buttons.addLink.addEventListener('click', this.handle.addLinkClicked);
        SharedUI.components.buttons.updateNode.addEventListener('click', this.handle.updateNodeClicked);
        SharedUI.components.buttons.updateLink.addEventListener('click', this.handle.updateLinkClicked);
        SharedUI.components.topMenu.newDiagramLink.addEventListener('click', this.handle.newDiagramClicked);
        SharedUI.components.topMenu.cloneDiagramLink.addEventListener('click', this.handle.cloneDiagramClicked);
        SharedUI.components.topMenu.saveToLocalStorage.addEventListener('click', this.handle.saveDiagramClicked);
        SharedUI.components.topMenu.saveToFileLink.addEventListener('click', this.handle.saveToFileClicked);
        SharedUI.components.topMenu.fileDialog.addEventListener('change', this.handle.loadFromFileClicked);
        SharedUI.components.topMenu.removeDiagramLink.addEventListener('click', this.handle.removeDiagramClicked);
        SharedUI.fields.linkFrom.addEventListener('change', this.handle.linkFromChanged);
        SharedUI.fields.linkTo.addEventListener('change', this.handle.linkToChanged);
        SharedUI.components.topMenu.open.addEventListener('mouseover', this.handle.fileOpenActivated);
        SharedUI.components.topMenu.openContent.addEventListener('mouseleave', this.handle.fileOpenDeactivated);

        SharedUI.fields.closeNav.addEventListener('click', this.handle.closeNav);
        SharedUI.fields.openNav.addEventListener('click', this.handle.openNav);
    }

    init() {
        SharedUI.reset()
        this.registerEvents();
    }

    get handle() {
        return {


            openNav: (e) => {

                SharedUI.openSidenav();

            },

            closeNav: (e) => { SharedUI.closeSidenav() },

            newDiagramClicked: (e) => {
                if (confirm('are you sure you want to discard any changes and start new diagram?')) {

                    var name = prompt('enter diagram name');
                    if (name !== null) {
                        SharedUI.reset();
                        this.diagramService.clearModel();
                        this.diagramService.storeDiagram(name);

                        SharedUI.fields.diagramNameInput.value = name
                    }
                }

                e.preventDefault();
            },

            removeDiagramClicked: (e) => {

                if (this.appState.isDiagramLoaded) {

                    if (confirm('are you sure you want to delete current diagram?')) {

                        //console.log('removing diagram of key', diagramName.value);
                        //this.removeDiagram(diagramName.value);
                        this.diagramService.removeDiagramByKey(this.currentDiagramName);
                        // this.clearInputs();
                        SharedUI.reset();
                        //  this.refreshAvailableDiagrams()
                    }
                } else {
                    alert('diagram name must not be empty\r\nplease load diagram before removing')
                }

                e.preventDefault();
            },

            saveDiagramClicked: (e) => {

                if (this.appState.isDiagramLoaded) {

                    //this.storeDiagram();
                    this.diagramService.storeDiagram(this.currentDiagramName);
                    //  this.refreshAvailableDiagrams();
                    alert(`changes to ${this.currentDiagramName} saved!`)

                } else {
                    alert('please enter diagram name')
                }


                e.preventDefault();
            },

            diagramSelectionChanged: (currentSelection) => {

                SharedUI.fields.diagramNameInput.value = currentSelection;
            },

            diagramChanged: (e) => {

                var nodes = this.diagramService.fetchNodes();
                SharedUI.refreshNodeLists(nodes);
                SharedUI.truncatePortLists();
                SharedUI.updateSavedModel(e.detail.model.toJson());
                SharedUI.resetHighlight();

            },

            nodeClicked: (e) => {

                if (e.detail.nodeData.items) {
                    this.dirty = e.detail.nodeData;

                    SharedUI.fields.selectedNodeNameInput.value = e.detail.nodeData.key;
                    SharedUI.fields.selecteNodeColorInput.value = e.detail.nodeData.color;
                    SharedUI.pushNodeItems(e.detail.nodeData.items, e.detail.nodeData);

                    if (e.detail.nodeData.category === 'note') {
                        SharedUI.fields.selectedNodeNameInput.disabled = true;
                    } else {
                        SharedUI.fields.selectedNodeNameInput.disabled = false;
                    }

                    SharedUI.highlightNodeSection();
                }

            },

            addNodeClicked: () => {
                var nodeData = SharedUI.components.newNodeData;
                this.diagramService.appendNode(nodeData);

            },

            removeNodeClicked: () => {
                //this.removeNode(); 
                var nodeKey = SharedUI.fields.selectedNodeNameInput.value;
                this.diagramService.removeNodeByKey(nodeKey);
            },

            linkClicked: (e) => {
                this.dirtyLink = e.detail.linkData;

                SharedUI.fields.relType.value = this.currentLink.relType;
                SharedUI.fields.linkFrom.value = this.currentLink.from;
                SharedUI.fields.linkTo.value = this.currentLink.to;
                SharedUI.fields.linkLabelInput.value = this.currentLink.linkLabel;

                var fromNode = this.diagramService.getNodeByKey(this.currentLink.from);
                SharedUI.refreshFromPorts(fromNode.items);

                var toNode = this.diagramService.getNodeByKey(this.currentLink.to);
                SharedUI.refreshToPorts(toNode.items);

                SharedUI.fields.fromPort.value = this.currentLink.fromPort;
                SharedUI.fields.toPort.value = this.currentLink.toPort;

                SharedUI.highlightLinkSection();

                // console.log('link clicked handler by app', this.currentLink);
            },

            addLinkClicked: () => {

                var data = SharedUI.components.linkData;
                this.diagramService.createLink(data);

            },

            updateNodeClicked: () => {

                var newNodeData = {

                    key: SharedUI.fields.selectedNodeNameInput.value,
                    color: SharedUI.fields.selecteNodeColorInput.value,
                    simple: this.current.simple,
                    entity: this.current.entity,
                    loc: this.current.loc,
                    items: SharedUI.retrieveNodeItems()
                }

                this.diagramService.updateNodeByKey(this.current.key, newNodeData);
            },

            updateLinkClicked: () => {

                var linkData = SharedUI.components.linkData;
                this.diagramService.updateLinkByKey(this.currentLink.key, linkData);
            },
            cloneDiagramClicked: (e) => {

                var currentName = SharedUI.fields.diagramNameInput.value;

                var name = prompt('enter diagram name', currentName);
                if (name !== null && name !== currentName) {

                    this.diagramService.storeDiagram(name);
                    this.diagramService.loadDiagram(name);

                    SharedUI.fields.diagramNameInput.value = name;

                }

                e.preventDefault();

            },

            loadFromFileClicked: (e) => {

                var self = this;
                var input = e.target;

                var reader = new FileReader();
                reader.onload = function() {
                    var data = reader.result;
                    self.diagramService.loadFromModel(data);
                    SharedUI.fields.diagramNameInput.value = input.files[0].name.replace('.json', '');

                };
                reader.readAsText(input.files[0]);
            },

            linkFromChanged: (e) => {

                var self = this;
                var node = self.diagramService.getNodeByKey(e.target.value);

                var items = node.items;
                SharedUI.refreshFromPorts(items);

            },

            linkToChanged: (e) => {
                var self = this;
                var node = self.diagramService.getNodeByKey(e.target.value);

                var items = node.items;

                SharedUI.refreshToPorts(items);

            },

            saveToFileClicked: (e) => {

                var json = SharedUI.components.savedModel.textContent;
                this.diagramService.saveToFile(json, e.target);

            },

            fileOpenActivated: (e) => {

                // Correct, but will not work with touch.
                var self = this;
                var subMenu = e.target.nextElementSibling; //document.getElementById('openSubMenu');
                subMenu.innerHTML = '';
                var diagrams = this.diagramService.fetchDiagrams();

                if (diagrams.length > 0) {

                    for (var key in diagrams) {

                        var li = document.createElement('li');
                        var a = document.createElement('a');

                        a.setAttribute('tabindex', '-1');
                        a.setAttribute('href', '#');
                        a.innerText = diagrams[key];
                        a.id = diagrams[key];
                        a.className = 'dropdown-item';
                        a.addEventListener('click', function(e) {

                            let key = this.id;
                            if (confirm(`you are going to load diagram ${key} \r\n.
                            Any unsaved changes to ${this.currentDiagramName || 
                            'current diagram '} will be lost. \r\n Proceed?`)) {
                                //this.loadDiagram(diagramKey);
                                self.diagramService.loadDiagram(key);
                                SharedUI.fields.diagramNameInput.value = key;

                            }

                        })
                        li.appendChild(a);
                        subMenu.appendChild(li);
                    }


                } else {
                    var li = document.createElement('li');
                    li.innerText = 'no diagrams saved';
                    subMenu.appendChild(li);
                }

                var bb = e.target.getBoundingClientRect();

                // subMenu.style.position = "fixed";
                subMenu.style.left = `${ bb.width+2}px`;
                subMenu.style.top = `${ bb.top- (bb.height*1.8)}px`
                subMenu.style.display = "block";

            },
            fileOpenDeactivated: (e) => {
                var subMenu = e.target;
                subMenu.style.display = "none";

            }

        }


    }


    tearDown() {
        this.diagramService.removeAllDiagrams();
    }

    buildExamples() {
        this.setupExample2('StudentExample1')
        this.setupExample3('StudentExample2')
    }

    setupExample1(name) {
        // this.diagramService.storeDiagram(name);
        this.diagramService.clearModel();
        SharedUI.fields.diagramNameInput.value = name

        var category = [{ name: "CategoryID", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "CategoryName", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "Description", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "TimeStamp", iskey: false, figure: "TriangleUp", dataType: "Date" },

            { name: "Quantity", iskey: false, figure: "TriangleUp", dataType: "Number" }
        ]

        var person = [{ name: "PersonId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "FirstName", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "LastName", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "CreatedTS", iskey: false, figure: "TriangleUp", dataType: "Date" },
            { name: "Picture", iskey: false, figure: "TriangleUp", dataType: "Blob" },

            { name: "DateOfBirth", iskey: false, figure: "TriangleUp", dataType: "Date" },
            { name: "PersonCategoryId", iskey: false, figure: "TriangleUp", dataType: "Number" },
            { name: "AddressId", iskey: false, figure: "TriangleUp", dataType: "Number" },
        ]


        var address = [{ name: "addressId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "City", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "Street", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "Building", iskey: false, figure: "TriangleUp", dataType: "Number" },
            { name: "CreatedTs", iskey: false, figure: "TriangleUp", dataType: "Date" },
            { name: "Active", iskey: false, figure: "TriangleUp", dataType: "Bool" },

        ]

        var categoryItems = [];
        var personItems = [];
        var addressItems = [];

        category.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            categoryItems.push(nodeItem);
        })
        person.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            personItems.push(nodeItem);
        })
        address.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            addressItems.push(nodeItem);
        })

        var n = SharedUI.components.newNodeData;
        n.key = "Category";
        n.color = SharedUI.colors.lightgrad;
        n.items = categoryItems;
        n.loc = "-200 400";
        n.simple = false;
        n.entity = true;

        this.addNode(n);

        var n1 = SharedUI.components.newNodeData;
        n1.key = "Person";
        n1.color = SharedUI.colors.lightgrad;
        n1.items = personItems;
        n1.loc = "-100 200";
        n1.simple = false;
        n1.entity = true;

        this.addNode(n1)

        var n2 = SharedUI.components.newNodeData;
        n2.key = "Address";
        n2.color = "#ffffff";
        n2.items = addressItems;
        n2.loc = "300 400";
        n2.simple = false;
        n2.entity = true;

        this.addNode(n2)



        var linkData = {
            from: n.key,
            fromPort: "CategoryID",
            to: n1.key,
            toPort: "PersonCategoryId",
            relType: "2",
            linkLabel: ""
        };


        var personAddress = {
            from: n1.key,
            fromPort: "AddressId",
            to: n2.key,
            toPort: "addressId",
            relType: "1",
            linkLabel: ""
        };

        this.addLink(linkData);
        this.addLink(personAddress)

        // this.diagramService.clearModel();

        this.diagramService.storeDiagram(name);

    }


    setupExample2(name) {
        // this.diagramService.storeDiagram(name);
        this.diagramService.clearModel();
        SharedUI.fields.diagramNameInput.value = name

        var classModel = [{ name: "courseName", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "sectionNumber", iskey: true, figure: "Cube1", dataType: "Number" },
            { name: "numRegistered", iskey: false, figure: "Cube1", dataType: "Number" },
            { name: "classDateTime", iskey: false, figure: "TriangleUp", dataType: "Date" },


        ]

        var courseModel = [{ name: "courseName", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "courseNumber", iskey: true, figure: "Cube1", dataType: "Number" },

        ]


        var studentModel = [{ name: "studentId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "studentName", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "studentAddress", iskey: false, figure: "Cube1", dataType: "Text" }


        ]

        var instructorModel = [{ name: "instructorNo", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "instructorName", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "instructorName", iskey: false, figure: "Cube1", dataType: "Text" }


        ]

        var sectionModel = [
            { name: "sectionNumber", iskey: true, figure: "Decision", dataType: "Number" }


        ]

        var professorModel = [{ name: "professorId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "professorName", iskey: true, figure: "Cube1", dataType: "Number" },
            { name: "professorFaculty", iskey: false, figure: "Cube1", dataType: "Number" }

        ]

        var classItems = [];
        var courseItems = [];
        var studentItems = [];
        var sectionItems = [];
        var professorItems = [];

        var instructorItems = [];

        classModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            classItems.push(nodeItem);
        })
        studentModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            studentItems.push(nodeItem);
        })
        courseModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            courseItems.push(nodeItem);
        })
        sectionModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            sectionItems.push(nodeItem);
        })
        professorModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            professorItems.push(nodeItem);
        })

        instructorModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            instructorItems.push(nodeItem);
        })

        var nodeData = {};
        var node = {};
        nodeData.key = "Student";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = studentItems;
        nodeData.loc = "-200 0";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);

        nodeData.key = "Class";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = classItems;
        nodeData.loc = "50 150";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);

        nodeData.key = "Course";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = courseItems;
        nodeData.loc = "50 -50";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);


        nodeData.key = "Instructor";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = instructorItems;
        nodeData.loc = "250 100";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);

        nodeData.key = "Section";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = sectionItems;
        nodeData.loc = "50 350";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);


        nodeData.key = "Professor";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = professorItems;
        nodeData.loc = "300 260";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);


        var studentCourse = {
            from: "Student",
            fromPort: "",
            to: "Course",
            toPort: "",
            relType: "4",
            linkLabel: "takes"
        };

        var link = this.linkFactory.createLink(studentCourse);
        this.diagramService.createLink(link);


        var courseClass = {
            from: "Course",
            fromPort: "",
            to: "Class",
            toPort: "",
            relType: "1",
            linkLabel: "has"
        };

        var link = this.linkFactory.createLink(courseClass);
        this.diagramService.createLink(link);


        var classSection = {
            from: "Section",
            fromPort: "",
            to: "Class",
            toPort: "",
            relType: "1",
            linkLabel: "has"
        };

        var link = this.linkFactory.createLink(classSection);
        this.diagramService.createLink(link);


        var courseInstructor = {
            from: "Instructor",
            fromPort: "",
            to: "Course",
            toPort: "",
            relType: "1",
            linkLabel: "teaches"
        };

        var link = this.linkFactory.createLink(courseInstructor);
        this.diagramService.createLink(link);


        var professorSection = {
            from: "Professor",
            fromPort: "",
            to: "Section",
            toPort: "",
            relType: "1",
            linkLabel: "teaches"
        };

        var link = this.linkFactory.createLink(professorSection);
        this.diagramService.createLink(link);





        this.diagramService.storeDiagram(name);

    }


    setupExample3(name) {
        // this.diagramService.storeDiagram(name);
        this.diagramService.clearModel();
        SharedUI.fields.diagramNameInput.value = name

        var marksModel = [{ name: "markId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "studentId", iskey: false, figure: "Cube1", dataType: "Number" },
            { name: "subjectId", iskey: false, figure: "Cube1", dataType: "Number" },
            { name: "date", iskey: false, figure: "TriangleUp", dataType: "Date" },
            { name: "mark", iskey: false, figure: "TriangleUp", dataType: "Number" }


        ]

        var subjectModel = [{ name: "subjectId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "title", iskey: true, figure: "Cube1", dataType: "Number" },

        ]

        var groupsModel = [{ name: "groupId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "name", iskey: true, figure: "Cube1", dataType: "Number" },

        ]


        var studentModel = [{ name: "studentId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "firstName", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "lastName", iskey: false, figure: "Cube1", dataType: "Text" },
            { name: "groupId", iskey: false, figure: "Cube1", dataType: "Number" }


        ]

        var subjectTeacherModel = [{ name: "subjectId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "teacherId", iskey: false, figure: "Cube1", dataType: "Number" },
            { name: "groupId", iskey: false, figure: "Cube1", dataType: "Number" }


        ]



        var teachersModel = [{ name: "teacherId", iskey: true, figure: "Decision", dataType: "Number" },
            { name: "firstName", iskey: true, figure: "Cube1", dataType: "Number" },
            { name: "lastName", iskey: false, figure: "Cube1", dataType: "Number" }

        ]

        var marksItems = [];
        var subjectItems = [];
        var studentItems = [];
        var groupsItems = [];
        var teachersItems = [];

        var subjectTeacherItems = [];

        marksModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            marksItems.push(nodeItem);
        })
        studentModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            studentItems.push(nodeItem);
        })
        subjectModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            subjectItems.push(nodeItem);
        })
        groupsModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            groupsItems.push(nodeItem);
        })
        teachersModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            teachersItems.push(nodeItem);
        })

        subjectTeacherModel.forEach(function(i) {
            var nodeItem = new NodeItem(i.name, i.dataType, i.iskey);
            subjectTeacherItems.push(nodeItem);
        })

        var nodeData = {};
        var node = {};
        nodeData.key = "Marks";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = marksItems;
        nodeData.loc = "200 -150";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);

        nodeData.key = "Students";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = studentItems;
        nodeData.loc = "-50 -70";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);

        nodeData.key = "Subjects";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = subjectItems;
        nodeData.loc = "0 150";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);


        nodeData.key = "Groups";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = groupsItems;
        nodeData.loc = "-200 200";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);

        nodeData.key = "SubjectsTeachers";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = subjectTeacherItems;
        nodeData.loc = "200 50";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);


        nodeData.key = "Teachers";
        nodeData.color = SharedUI.colors.lightgrad;
        nodeData.items = teachersItems;
        nodeData.loc = "180 260";
        nodeData.simple = false;
        nodeData.entity = true;

        //this.addNode(n);

        node = this.nodeFactory.createNode(nodeData);
        this.diagramService.appendNode(node);


        var studentMarks = {
            from: "Students",
            fromPort: "studentId",
            to: "Marks",
            toPort: "studentId",
            relType: "1",
            linkLabel: ""
        };

        var link = this.linkFactory.createLink(studentMarks);
        this.diagramService.createLink(link);


        var groupsStudents = {
            from: "Groups",
            fromPort: "groupId",
            to: "Students",
            toPort: "groupId",
            relType: "1",
            linkLabel: ""
        };

        var link = this.linkFactory.createLink(groupsStudents);
        this.diagramService.createLink(link);


        var groupsSubjectTeachers = {
            from: "Groups",
            fromPort: "groupId",
            to: "SubhectsTeachers",
            toPort: "groupdId",
            relType: "1",
            linkLabel: ""
        };

        var link = this.linkFactory.createLink(groupsSubjectTeachers);
        this.diagramService.createLink(link);


        var subjectsSubjectsTeachers = {
            from: "Subjects",
            fromPort: "subjectId",
            to: "SubjectsTeachers",
            toPort: "subjectId",
            relType: "1",
            linkLabel: ""
        };

        var link = this.linkFactory.createLink(subjectsSubjectsTeachers);
        this.diagramService.createLink(link);


        var subjectsMarks = {
            from: "Subjects",
            fromPort: "subjectId",
            to: "Marks",
            toPort: "subjectId",
            relType: "1",
            linkLabel: ""
        };

        var link = this.linkFactory.createLink(subjectsMarks);
        this.diagramService.createLink(link);


        var teachersSubjectsTeachers = {
            from: "Teachers",
            fromPort: "teacherId",
            to: "SubjectsTeachers",
            toPort: "teacherId",
            relType: "1",
            linkLabel: ""
        };

        var link = this.linkFactory.createLink(teachersSubjectsTeachers);
        this.diagramService.createLink(link);

        this.diagramService.storeDiagram(name);

    }



}