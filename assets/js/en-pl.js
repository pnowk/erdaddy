var linguJSON = {
    "languages": [{
        "lang_name": "English",
        "lang_code": "en",
        "url_pattern": "?"
    }, {
        "lang_name": "Polish",
        "lang_code": "pl",
        "url_pattern": "?"
    }],
    "translated_segments": [{
            "source": "File",
            "target": "Plik"
        }, {
            "source": "New",
            "target": "sea encontrado por los motores de búsqueda en otros idiomas",
        }, {
            "source": "Powered by <strong>Lingumania</strong>",
            "target": "Potenciado por <strong>Lingumania</strong>"
        },

        {
            "source": "New...",
            "target": "Nowy..."
        },
        {
            "source": "Open...",
            "target": "Otwórz..."
        },
        {
            "source": "Rename...",
            "target": "Zmień nazwę..."
        },


        {
            "source": "Save...",
            "target": "Zapisz..."
        },

        {
            "source": "Save to File",
            "target": "Zapisz do Pliku"
        },
        {
            "source": "Delete Current Diagram...",
            "target": "Usuń diagram..."
        },
        {
            "source": "Save...",
            "target": "Zapisz..."
        },


        {
            "source": " Save",
            "target": "Zapisz"
        },

        {
            "source": "Add",
            "target": "Dodaj"
        },

        {
            "source": "Delete",
            "target": "Usuń"
        },

        {
            "source": "Remove",
            "target": "Usuń"
        },

        {
            "source": "Update",
            "target": "Zaktualizuj"
        },

        {
            "source": "Create",
            "target": "Utwórz"
        },

        {
            "source": "node name",
            "target": "nazwa"
        },

        {
            "source": "selecte node",
            "target": "nazwa"
        },

        {
            "source": "Selected Node:",
            "target": "Wybrany węzeł"
        },

        {
            "source": "Create New Node:",
            "target": "Utwórz węzeł"
        },

        {
            "source": "Relationship:",
            "target": "Relacja"
        },

        {
            "source": "Node Color",
            "target": "Kolor węzła"
        },

        {
            "source": "Simple",
            "target": "Prosty"
        },
        {
            "source": "Entity",
            "target": "Encja"
        },

        {
            "source": "Node Name",
            "target": "Nazwa węzła"
        },

        {
            "source": " New Item",
            "target": "Nowe Pole"
        },

        {
            "source": "one to many",
            "target": "jeden do wielu"
        },
        {
            "source": "one to one",
            "target": "jeden to jednego"
        },
        {
            "source": "many to many",
            "target": "wiele do wielu"
        },
        {
            "source": " New Item",
            "target": "Nowe Pole"
        },
        {
            "source": "plain",
            "target": "Linia"
        },
        {
            "source": "standard",
            "target": "Kierunek"
        },


        {
            "source": "From",
            "target": "Od"
        },

        {
            "source": "To",
            "target": "Do"
        },


        {
            "source": "Type",
            "target": "Typ relacji"
        },

        {
            "source": "Label",
            "target": "Tekst"
        },

        {
            "source": "Database",
            "target": "Baza danych"
        },

        {
            "source": "Actor",
            "target": "Aktor"
        },

        {
            "source": "Note",
            "target": "Notatka"
        },

        {
            "source": "Process",
            "target": "Proces"
        },

        {
            "source": "Decision",
            "target": "Decyzja"
        },

        {
            "source": "Ellipse",
            "target": "Elipsa"
        },

        {
            "source": "association",
            "target": "Asocjacja"
        },

        {
            "source": "generalization",
            "target": "Uogólnienie"
        },

        {
            "source": "composition",
            "target": "Kompozycja"
        },

        {
            "source": "aggregation",
            "target": "Agregacja"
        },

        {
            "source": "* select entity and field",
            "target": "wybierz encję oraz pole encji [opcjonalnie]"
        },

        {
            "source": "* field name, data type, is key",
            "target": "nazwa pola, typ danych, czy klucz"
        },

        {
            "source": "Clone...",
            "target": "Klonuj diagram..."
        },





    ]
};