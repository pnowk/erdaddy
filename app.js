const express = require('express')
const app = express()
const port = 80



app.use(express.static('assets'))

app.set('view engine', 'ejs')


//console.log(process.env.NODE_ENV);
app.get('/', function(req, res) {
    res.render('index', { title: 'ERDaddy', message: 'Hello there!' })

})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))